// Почему для работы с input не рекомендуется использовать события клавиатуры
// Потому что их можно обойти, вставить текст, ввести по голосовому вводу.

/*
В файле index.html лежит разметка для кнопок.
    Каждая кнопка содержит в себе название клавиши на клавиатуре
По нажатию указанных клавиш - та кнопка, на которой написана эта буква,
    должна окрашиваться в синий цвет. При этом, если какая-то другая буква
уже ранее была окрашена в синий цвет - она становится черной. Например по
нажатию Enter первая кнопка окрашивается в синий цвет. Далее, пользователь
нажимает S, и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной.*/

/*const input = document.addEventListener('keyup', event => {
    input.classList.add('active');
    const button = document.querySelectorAll('#btn');
    console.log(input);
});*/

let allKeys = document.querySelectorAll('.btn');
console.log(allKeys);

function selectKey() {
    window.addEventListener("keydown", (event) => {
        const key = event.key;
        for (const btn of allKeys) {
            btn.classList.remove("active");
            if (btn.innerText === key) {
                btn.classList.add("active");
            }
        }
    });
}

selectKey();

document.addEventListener('keydown', function(event) {
    console.log(event.key)
});