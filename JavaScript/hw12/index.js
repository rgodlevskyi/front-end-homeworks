// Опишите своими словами разницу между функциями setTimeout() и setInterval().
// Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

// setTimeout() - время до выполнения 1 задержка. setInterval() - задержка с интервалами, нужное кол-во раз
// функция setTimeout() сработает сразу
// чтобы интервал перестал повторяться

const imageWrapper = document.querySelectorAll('.image-to-show');
const pauseBtn = document.querySelector('#pause');
const resumeBtn = document.querySelector('#resume');

let currentImageIndex = 0;

function changeImages() {
    imageWrapper[currentImageIndex++].hidden = true;
    if (currentImageIndex === imageWrapper.length) currentImageIndex = 0;
    imageWrapper[currentImageIndex].hidden = false;
}

let update = setInterval(changeImages, 3000);

pauseBtn.addEventListener('click', () => {
    clearInterval(update);
    update = false;
});

resumeBtn.addEventListener('click', () => {
    if (!update) update = setInterval(changeImages, 3000);
});
