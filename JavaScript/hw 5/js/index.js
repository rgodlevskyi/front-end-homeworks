/* Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
    Экранирование - помогает добавить символы как строку, без ихних свойств.
*/

// переменная объекте - это поля
// функция в объекте - это метод

function createNewUser() {
    let newUser = {}
    newUser.firstName = prompt('Введите имя');
    newUser.lastName = prompt('Введите фамилию');
    let birthday = prompt('Дата рождения: через точку');
    let array = birthday.split('.');
    let buffer = array[0];
    array[0] = array[1];
    array[1] = buffer;
    birthday = array.join('.');
    let result = new Date(birthday);
    if (result) {
        newUser.birthday = result;
    }
    newUser.getAge = function() {
        return (new Date(new Date() - this.birthday)).getFullYear() - 1970;
    }
    newUser.getPassword = function() {
        return newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + this.birthday.getFullYear();
    }
    newUser.getLogin = function() {
        return (this.firstName[0] + this.lastName).toLowerCase();
    }
    return newUser;
};

console.log(createNewUser().getPassword());