const arrCategory = [
    'Graphic Design',
    'Web Design',
    'Landing Pages',
    'Wordpress'
];

const arrWorks = [
    {
        name: 'CREATIVE DESIGN',
        category: 0,
        img: 'img/our_amazing_work/5.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 1,
        img: 'img/our_amazing_work/1.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 3,
        img: 'img/our_amazing_work/2.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 2,
        img: 'img/our_amazing_work/3.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 3,
        img: 'img/our_amazing_work/4.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 3,
        img: 'img/our_amazing_work/5.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 2,
        img: 'img/our_amazing_work/6.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 1,
        img: 'img/our_amazing_work/7.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 1,
        img: 'img/our_amazing_work/8.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 0,
        img: 'img/our_amazing_work/9.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 2,
        img: 'img/our_amazing_work/10.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 0,
        img: 'img/our_amazing_work/11.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 0,
        img: 'img/our_amazing_work/5.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 1,
        img: 'img/our_amazing_work/1.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 3,
        img: 'img/our_amazing_work/2.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 2,
        img: 'img/our_amazing_work/3.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 3,
        img: 'img/our_amazing_work/4.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 3,
        img: 'img/our_amazing_work/5.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 2,
        img: 'img/our_amazing_work/6.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 1,
        img: 'img/our_amazing_work/7.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 1,
        img: 'img/our_amazing_work/8.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 0,
        img: 'img/our_amazing_work/9.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 2,
        img: 'img/our_amazing_work/10.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 0,
        img: 'img/our_amazing_work/11.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 0,
        img: 'img/our_amazing_work/5.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 1,
        img: 'img/our_amazing_work/1.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 3,
        img: 'img/our_amazing_work/2.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 2,
        img: 'img/our_amazing_work/3.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 3,
        img: 'img/our_amazing_work/4.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 3,
        img: 'img/our_amazing_work/5.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 2,
        img: 'img/our_amazing_work/6.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 1,
        img: 'img/our_amazing_work/7.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 1,
        img: 'img/our_amazing_work/8.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 0,
        img: 'img/our_amazing_work/9.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 2,
        img: 'img/our_amazing_work/10.png',
    },
    {
        name: 'CREATIVE DESIGN',
        category: 0,
        img: 'img/our_amazing_work/11.png',
    },
];

const workList = document.querySelector('#work_list');
const workTemp = document.querySelector('template[name=work_card]');
const workTabs = document.querySelector('#work_tabs');
const tabTemp = document.querySelector('template[name=work_tab]');
const moreWork = document.querySelector('#more_work');

const workLoader = document.querySelector('#loader_works');

let limitCards = 12;

function renderWorkTab(tabName, id) {
    let clone = tabTemp.content.cloneNode(true).childNodes[1];
    clone.innerText = tabName;
    clone.dataset.id = id;
    workTabs.appendChild(clone);
}

renderWorkTab('All', -1);
for (let i = 0; i < arrCategory.length; i++) {
    renderWorkTab(arrCategory[i], i);
}

function renderWorks(works) {
    workList.innerHTML = '';
    for (const work of works) {
        let clone = workTemp.content.cloneNode(true).childNodes[1];
        let img = clone.querySelector('img');
        let h1 = clone.querySelector('h1');
        let p = clone.querySelector('p');
        img.src = work.img;
        h1.innerText = work.name;
        p.innerText = arrCategory[work.category];
        workList.appendChild(clone);
    }
}

renderWorks(arrWorks.slice(0, limitCards));

workTabs.addEventListener('click', (event) => {
    let target = event.target;
    if (target.tagName === 'A') {
        let works = arrWorks.slice(0, limitCards);
        if (target.dataset.id >= 0) {
            works = works.filter((item) => item.category == target.dataset.id);
        }
        renderWorks(works);
    }
});

function changeLimit(target) {
    limitCards += 12;
    renderWorks(arrWorks.slice(0, limitCards));
    if (limitCards >= 36) {
        target.style.display = 'none';
    } else {
        target.style.display = 'flex';
    }
    workLoader.classList.remove('loader');
}

moreWork.addEventListener('click', (event) => {
    workLoader.classList.add('loader');

    event.target.style.display = 'none';

    setTimeout(changeLimit, 3000, event.target);
});