const arrFeedback = [
    {
        name: 'Reza Bahrami',
        job: 'Photographer/Filmmaker',
        feedback: 'TheHam is amazingly responsive and really cares about the client. It\'s the best online service that I have ever used!',
        avatar: 'img/what_people_say_about_theham/1.png'
    },
    {
        name: 'Michel Vivas',
        job: 'Senior Technology OfficerFrom',
        feedback: 'TheHam has helped me to improve my written skills as well as to communicate more naturally, like a local English speaker.',
        avatar: 'img/what_people_say_about_theham/2.png'
    },
    {
        name: 'Hasan Ali',
        job: 'UX Designer',
        feedback: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        avatar: 'img/what_people_say_about_theham/3.png'
    },
    {
        name: 'Chiara Baesso',
        job: 'Copywriter',
        feedback: 'I started to use TheHam when I began to learn English. It has been an awesome way to improve my English skills.',
        avatar: 'img/what_people_say_about_theham/4.png'
    }
];

const name = document.querySelector('#name');
const job = document.querySelector('#job');
const feedback = document.querySelector('#feedback');
const avatarMain = document.querySelector('#main_img');
const listImage = document.querySelector('#list_img');
const avatarTemp = document.querySelector('template[name=feedback_avatar]');

const leftBtn = document.querySelector('#left_btn');
const rightBtn = document.querySelector('#right_btn');

for (let i = 0; i < arrFeedback.length; i++) {
    let clone = avatarTemp.content.cloneNode(true).childNodes[1];
    clone.src = arrFeedback[i].avatar;
    clone.dataset.id = i;
    listImage.appendChild(clone);
}

function changeFeedback(index) {
    name.innerText = arrFeedback[index].name;
    job.innerText = arrFeedback[index].job;
    feedback.innerText = arrFeedback[index].feedback;
    avatarMain.src = arrFeedback[index].avatar;
    lastImage.classList.remove('active-image');
    lastImage = images[index];
    lastImage.classList.add('active-image');
}

let feedbackIndex = 0;
const images = listImage.children;
let lastImage = images[0];
changeFeedback(0);

leftBtn.addEventListener('click', () => {
    feedbackIndex--;
    if (feedbackIndex < 0) feedbackIndex = arrFeedback.length - 1;
    changeFeedback(feedbackIndex);
});

rightBtn.addEventListener('click', () => {
    feedbackIndex++;
    if (feedbackIndex >= arrFeedback.length) feedbackIndex = 0;
    changeFeedback(feedbackIndex);
});

listImage.addEventListener('click', (event) => {
    if (event.target.tagName === 'IMG') {
        feedbackIndex = event.target.dataset.id;
        changeFeedback(feedbackIndex);
    }
});