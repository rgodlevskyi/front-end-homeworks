const galleryGrid = document.querySelector('#gallery_grid');
const galleryButton = document.querySelector('#more_gallery');
const galleryLoader = document.querySelector('#loader_gallery');

function addMore() {
    let child = galleryGrid.children;
    for (let i = 0; i < child.length; i++) {
        galleryGrid.childNodes[i*2+1].appendChild(child[i].cloneNode(true));
    }

    galleryLoader.classList.remove('loader');
}

galleryButton.addEventListener('click', () => {
    galleryLoader.classList.add('loader');
    galleryButton.style.display = 'none';

    setTimeout(addMore, 3000);
});