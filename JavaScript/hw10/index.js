let icons = document.querySelectorAll('.fas');
icons.forEach(item =>{
    item.addEventListener('click',selectIcon)
})

function selectIcon(event){
    console.log(event.target);
    let target = event.target;
    target.classList.toggle('fa-eye-slash');
    if (target.classList.contains('fa-eye-slash')) {
        (target.previousElementSibling.type = 'text')
    } else {
        (target.previousElementSibling.type = 'password')
    }
}
let div = document.createElement('span');
div.innerHTML = 'Нужно ввести одинаковые значения';
div.style.color = 'red';
let info = document.querySelector('.info');
let input = document.querySelector('.input-one');
let input2 = document.querySelector('.input-two');
let button = document.querySelector('.btn');

button.addEventListener('click', (e) => {
    e.preventDefault();
    if (input.value !== input2.value || input.value === "") {
        info.append(div);
    } else {
        if (info.hasChildNodes) {
            info.childNodes.forEach(item => item.remove())
        }
        // setTimeout добавил, чтобы после неправильной попытки сабмита,
        // после которой появляется текст с ошибкой. думаю, что этот текст должен убираться во время алерта,
        // а не после нажатия "ок".
        // Возможно есть решение получше, но я его не знаю пока что, нашел только вот такой "костыль".
        // Благодаря задержке в 0 секунд, красный текст пропадает когда нужно, то есть при подтверждении во время появления алерта
        setTimeout(function() {
            alert('You are welcome');
            console.log(button);
        },0);
    }
});