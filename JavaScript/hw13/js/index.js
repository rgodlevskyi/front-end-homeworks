let localStorage = window.localStorage;
let root = document.querySelector(':root');
let currentTheme = localStorage.getItem('current_theme');

if (currentTheme === null) currentTheme = 'light';
if (currentTheme === 'light') {
    lightTheme(root);
} else {
    darkTheme(root);
}

function lightTheme(root) {
    root.style.setProperty('--bg-color', 'white');
    root.style.setProperty('--text-color', 'black');
}

function darkTheme(root) {
    root.style.setProperty('--bg-color', 'black');
    root.style.setProperty('--text-color', 'white');
}

function changeTheme() {
    if (currentTheme === 'light') {
        darkTheme(root);
        currentTheme = 'dark';
    } else {
        lightTheme(root);
        currentTheme = 'light';
    }
    localStorage.setItem('current_theme', currentTheme);
}