const tabMenu = document.querySelector(".tabs");
tabMenu.addEventListener("click", (event) => {
    let target = event.target;
    if (target.tagName === "LI") {
        change(target);
    }
});
const change = (target) => {
    let menuItems = document.querySelectorAll(".tabs-title");
    for (const item of menuItems) {
        item.classList.remove('active');
    }
    target.classList.add("active");
    let id = target.dataset.id;
    let list = document.querySelector('.tabs-content');
    let arr = list.children;
    for (let i = 0; i < arr.length; i++) {
        if (i == id) {
            arr[i].hidden = false;
        } else {
            arr[i].hidden = true;
        }
    }
};