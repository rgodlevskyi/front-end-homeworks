let allKeys = document.querySelectorAll('.btn');
console.log(allKeys);

function selectKey() {
    window.addEventListener("keydown", (event) => {
        const key = event.key;
        for (const btn of allKeys) {
            btn.classList.remove("active");
            if (btn.innerText === key) {
                btn.classList.add("active");
            }
        }
    });
}

selectKey();

document.addEventListener('keydown', function(event) {
    console.log(event.key)
});