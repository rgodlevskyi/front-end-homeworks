// HW2 - find-multiples

// Циклы нужны, чтобы уменьшить кол-во написания строк кода.
// Циклы выполняют код нужное кол-во раз, пока все условия не будут выполнены до конца.

let userNumber = +prompt("Введите число", "");
if (userNumber < 5) {
    alert("Sorry, no numbers");
} else {
    for (let i = 0; i <= userNumber; i += 5) {
        console.log(i);
    }
}
