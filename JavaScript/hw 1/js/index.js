/*
1. Объяснить своими словами разницу между объявлением переменных через var, let и const.
var - есть хостинг, всегда поднимается вверх, можно вызвать из блока, можно менять значение переменной
let - видна только внутри блока, можно менять значение переменной
const - как константа, ее прямое значение нельзя изменить. Но можно изменить значение ее функции, массива, объекта.

2. Почему объявлять переменную через var считается плохим тоном?
var - устаревшая переменная, больше не используется по новым стандартам синтаксис Ecma Script 6 или что там ...
 */


let name = '';
let age = null;

while(!Number.isInteger(age) || age < 1 || name === '') {
    name = prompt('Enter your name');
    age = +prompt('How old are you?');
}

if (age < 18 || age <= 22 && !confirm('Are you sure you want to continue?')) {
    alert('You are not allowed to visit this website');
} else {
    alert(`Welcome ${name}!`);
}