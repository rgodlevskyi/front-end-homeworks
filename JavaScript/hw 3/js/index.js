/* HW3 - math-function

1. Описать своими словами для чего вообще нужны функции в программировании.
 Функции нужны для того, чтобы быстро вызывать ранее написанный код, n-ое кол-во раз, в любом месте кода
 (повторяющийся код, операции, операнды, параметры, выражения, и т.д.)

2. Описать своими словами, зачем в функцию передавать аргумент.
 Чтобы потом дать им значение и записать их внутри функции
*/

let firstNum;
let secondNum;

while (!Number.isInteger(firstNum) || !Number.isInteger(secondNum) || secondNum == 0){
    firstNum = +prompt("Hello! Enter the first number");
    secondNum = +prompt("Enter the second number");
}

let mathAction = prompt("Chose one of this math operations: +, -, *, /");

function operation(firstNum, secondNum, mathAction){
    switch (mathAction){
        case "+":
            return firstNum + secondNum;
        case "-":
            return firstNum - secondNum;
        case "*":
            return firstNum * secondNum;
        case "/":
            return firstNum / secondNum;
        default:
            return null;
    }
}

console.log(operation(firstNum, secondNum, mathAction));
