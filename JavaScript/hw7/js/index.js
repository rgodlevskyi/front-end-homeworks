/* Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
 Document Object Model (DOM) - содержит внутри себя всё, что есть на странице в виде объектов, которыми можно управлять
 */

    const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

    function addList(arr, parent = document.body) {
    parent.innerHTML += `<ul>${arr.map(item => `<li>${item}</li>`).join('')}</ul>`;
}
addList(arr);
